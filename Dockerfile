# Working from last ubuntu lts that we have tested
FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

# Install required
RUN apt-get -y update && \
apt-get install -y \
libmicrohttpd-dev \
libjansson-dev \
libssl-dev \
libsofia-sip-ua-dev \
libglib2.0-dev \
libopus-dev \
libogg-dev \
libcurl4-openssl-dev \
liblua5.3-dev \
libconfig-dev \
pkg-config \
gengetopt \
libtool \
automake \
make \
meson \
cmake \
aptitude \
build-essential \
ffmpeg \
git \
wget

# Install libnice
RUN cd /tmp && \
git clone https://gitlab.freedesktop.org/libnice/libnice && \
cd libnice && \
meson --prefix=/usr build && ninja -C build && ninja -C build install

# Install libsrtp
RUN cd /tmp && \ 
wget https://github.com/cisco/libsrtp/archive/v2.2.0.tar.gz && \
tar xfv v2.2.0.tar.gz && \
cd libsrtp-2.2.0 && \
./configure --prefix=/usr --enable-openssl && \
make shared_library && make install

# Install libwebsockets
RUN cd /tmp && \
git clone https://github.com/warmcat/libwebsockets.git && \
cd libwebsockets && \
mkdir build && \
cd build && \
cmake -DLWS_MAX_SMP=1 -DCMAKE_INSTALL_PREFIX:PATH=/usr -DCMAKE_C_FLAGS="-fpic" .. && \
make && make install

# Install usrsctp
RUN cd /tmp && \
git clone https://github.com/sctplab/usrsctp && \
cd usrsctp && \
./bootstrap && \
./configure --prefix=/usr && make && make install

# Install janus-gateway
RUN cd /tmp && \
git clone https://github.com/meetecho/janus-gateway.git && \
cd janus-gateway && \
sh autogen.sh && \
./configure --prefix=/opt/janus && \
make && make install && \
make configs

#Copy janus custom .jcfg and certificates
COPY ./janus /opt/janus
COPY ./letsencrypt /etc/letsencrypt

EXPOSE 10000-10200/udp
EXPOSE 8188
EXPOSE 8088
EXPOSE 8089
EXPOSE 8889
EXPOSE 8000
EXPOSE 7088
EXPOSE 7089

CMD ["/opt/janus/bin/janus"]
